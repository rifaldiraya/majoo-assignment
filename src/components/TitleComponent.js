import React from "react";
import { Row, Col } from "antd";

export default function TitleComponent(props) {
  return (
    <>
      <div className="title-comp">
        <Row>
          <Col span={24}>
            <div className="title">
              <span style={{ color: props.tCol }}>{props.title}</span>
            </div>
            <div>
              <span style={{ color: props.tCol }} className="desc">
                {props.desc}
              </span>
            </div>
          </Col>
        </Row>
      </div>
    </>
  );
}
