import React from "react";
import { Switch, Route } from "react-router-dom";
import ToDoList from "../pages/ToDoList/ToDoList";
import * as routes from "./Routes";

export default function RoutePages(props) {
  return (
    <Switch>
      <Route exact path={routes.LANDING} component={ToDoList} />
      <Route exact path={routes.TO_DO_LIST} component={ToDoList} />
    </Switch>
  );
}
