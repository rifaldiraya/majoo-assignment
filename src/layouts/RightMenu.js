import React from "react";
import { Link } from "react-router-dom";
import * as routes from "../config/Routes";

export default function RightMenu() {
  return (
    <div className="rightMenu">
      <Link style={{ color: "black", paddingRight: "20px", fontSize: "24px" }} to={routes.ABOUT}>
        <span>About</span>
      </Link>
    </div>
  );
}
