import React from "react";
import { Layout, Image } from "antd";
import { Link } from "react-router-dom";
import * as routes from "../config/Routes";

const { Header } = Layout;

export default function MainHeader() {
  return (
    <div style={{ margin: "12px 0px 12px 0px" }}>
      <Layout>
        <Header style={{ textAlign: "center", zIndex: 1, width: "100%", color: "black", backgroundColor: "white" }}>
          <Link to={routes.TO_DO_LIST}>
            <Image style={{ padding: 10 }} preview={false} width="auto" src="majoo.png" />
          </Link>
        </Header>
      </Layout>
    </div>
  );
}
