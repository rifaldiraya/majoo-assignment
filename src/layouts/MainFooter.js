import React from "react";
import { Layout } from "antd";
import { CopyrightOutlined } from "@ant-design/icons";

const { Footer } = Layout;

export default function MainFooter() {
  return (
    <>
      <div>
        <Footer style={{ textAlign: "center", fontSize: "20px" }} className="main-footer">
          <span>
            Created by Majoo Indonesia <CopyrightOutlined style={{ paddingLeft: "6px" }} /> 2021
          </span>
        </Footer>
      </div>
    </>
  );
}
