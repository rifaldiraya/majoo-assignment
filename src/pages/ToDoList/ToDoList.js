import React from "react";
import TitleComponent from "../../components/TitleComponent";
import ToDoLayouts from "./ToDoLayouts";

export default function ToDoList(props) {
  return (
    <div style={{ backgroundColor: "grey" }}>
      <div className="todopage">
        <TitleComponent tCol="#ffff" title="To Do List" desc="Break It Up, do all your task and get more productive every day" />
      </div>
      <ToDoLayouts />
    </div>
  );
}
