import React, { useState, useImperativeHandle, forwardRef, useEffect } from "react";
import { Drawer, Row, Col, Form, Input, Select, Button } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { addData } from "redux/action/userAction";
import moment from "moment";

const DrawerAddItem = forwardRef((props, ref) => {
  let dispatch = useDispatch();
  const [form] = Form.useForm();

  const [visible, setVisible] = useState();
  const { getDataAPI } = useSelector((state) => state.data);

  useEffect(() => {
    if (visible) {
      form.resetFields();
    }
  }, [form, visible]);

  useImperativeHandle(ref, () => ({
    showDrawer(data) {
      setVisible(true);
    },
  }));

  const closeDrawer = () => {
    setVisible(false);
  };

  const onFinish = (val) => {
    dispatch(addData([{ ...val, id: getDataAPI && getDataAPI[getDataAPI.length - 1].id + 1, createdAt: moment().format("YYYY-MM-DD hh:mm") }], getDataAPI));
    setVisible(false);
  };

  return (
    <Drawer
      title="Add New Item"
      closable={false}
      placement="right"
      visible={visible}
      onClose={closeDrawer}
      width="40%"
      footer={
        <>
          <div style={{ float: "left" }}>
            <Button type="default" onClick={closeDrawer}>
              Cancel
            </Button>
          </div>
          <div style={{ float: "right" }}>
            <Button onClick={form.submit} type="primary">
              Submit
            </Button>
          </div>
        </>
      }
    >
      <Form form={form} layout="vertical" onFinish={onFinish}>
        <Form.Item label="Title" name="title">
          <Input />
        </Form.Item>
        <Form.Item label="Description" name="description">
          <Input />
        </Form.Item>
        <Row gutter={24}>
          <Col span={12}>
            <Form.Item label="Status" name="status" valuePropName="checked">
              <Select placeholder="Change Status" optionFilterProp="children" filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
                <Select.Option value={0}>Active</Select.Option>
                <Select.Option value={1}>Done</Select.Option>
              </Select>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Drawer>
  );
});

export default DrawerAddItem;
