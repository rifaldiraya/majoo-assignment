import React, { useState, useImperativeHandle, forwardRef } from "react";
import { Drawer, Row, Col, Divider } from "antd";
import { ClockCircleOutlined, FieldNumberOutlined } from "@ant-design/icons";

const DrawerDetailItem = forwardRef((props, ref) => {
  const [visible, setVisible] = useState();
  const [todoListItem, setTodoListItem] = useState();

  useImperativeHandle(ref, () => ({
    showDrawer(data) {
      setTodoListItem(data);
      setVisible(true);
    },
  }));

  const closeDrawer = () => {
    setVisible(false);
  };

  return (
    <Drawer title="Item Details" closable={false} placement="right" visible={visible} onClose={closeDrawer} width="40%">
      <Row>
        <Col span={12} className="itemPanel">
          <div className="icon-style">
            <FieldNumberOutlined className="icon-display" />
          </div>
          <div className="fontPanel">User Id</div>
          <span className="card-title">#{todoListItem && todoListItem.id}</span>
        </Col>
        <Col span={12} className="itemPanel">
          <div className="icon-style">
            <ClockCircleOutlined className="icon-display" />
          </div>
          <div className="fontPanel">Created At</div>
          <span className="card-title">{todoListItem && todoListItem.createdAt}</span>
        </Col>
      </Row>
      <Divider style={{ color: "rgba(0,0,0,.5)" }} orientation="left">
        <b>Title & Description</b>
      </Divider>
      <Row>
        <Col span={24} className="itemPanel">
          <div className="fontPanel">Title</div>
          <span className="card-title">{todoListItem && todoListItem.title}</span>
        </Col>
      </Row>
      <Row>
        <Col span={24} className="itemPanel">
          <div className="fontPanel">Description</div>
          <span className="card-title">{todoListItem && todoListItem.description}</span>
        </Col>
      </Row>
    </Drawer>
  );
});

export default DrawerDetailItem;
