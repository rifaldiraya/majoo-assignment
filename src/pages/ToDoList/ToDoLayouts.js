import React, { useRef, useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getData, deleteData } from "redux/action/userAction";
import { Col, Row, Modal, Tabs, Button } from "antd";
import { FieldNumberOutlined, PlusOutlined, ClockCircleOutlined, HighlightOutlined, AlignCenterOutlined, EditOutlined, DeleteOutlined } from "@ant-design/icons";
import DrawerDetailItem from "./DrawerDetailItem";
import DrawerUpdateItem from "./DrawerUpdateItem";
import DrawerAddItem from "./DrawerAddItem";

const { TabPane } = Tabs;

export default function ToDoLayouts(props) {
  const { getDataAPI } = useSelector((state) => state.data);
  const { getReverse } = useSelector((state) => state.data);
  let dispatch = useDispatch();
  const [modalAppear, setModalAppear] = useState();
  const [deleteHelper, setDeleteHelper] = useState();
  const drawerDetailItem = useRef();
  const drawerUpdateItem = useRef();
  const drawerAddItem = useRef();

  const openDrawerDetail = (item) => {
    drawerDetailItem.current.showDrawer(item);
  };

  const openDrawerUpdate = (item) => {
    drawerUpdateItem.current.showDrawer(item);
  };

  const openDrawerAddItem = () => {
    drawerAddItem.current.showDrawer();
  };

  const handleCancel = () => {
    setModalAppear(false);
  };

  const displayModal = (id) => {
    setDeleteHelper(id);
    setModalAppear(true);
  };

  const deleteItem = () => {
    dispatch(deleteData(deleteHelper, getDataAPI));
    setModalAppear(false);
  };

  useEffect(() => {
    dispatch(getData());
  }, [dispatch]);

  useEffect(() => {
    if (getDataAPI) {
      return getDataAPI;
    }
  }, [getDataAPI]);

  return (
    <div className="bkl">
      <Tabs defaultActiveKey="1" tabPosition="top" type="card">
        <TabPane tab="Active" key="1">
          {getDataAPI &&
            getDataAPI.map((item, index) => (
              <>
                {item && item.status === 0 ? (
                  <>
                    <Row className="panel" style={{ cursor: "pointer", padding: "15px" }}>
                      <Col onClick={() => openDrawerDetail(item)} span={22}>
                        <Row>
                          <Col span={3} style={{ paddingLeft: "15px" }}>
                            <div className="icon-style">
                              <FieldNumberOutlined className="icon-display" />
                            </div>
                            <span className="card-title">
                              <b>#{item.id}</b>
                            </span>
                            <div className="fontPanel">User Id</div>
                          </Col>
                          <Col span={6} style={{ paddingLeft: "15px" }}>
                            <div className="icon-style">
                              <HighlightOutlined className="icon-display" />
                            </div>
                            <span className="card-title">
                              <b>{item.title.length > 12 ? item.title.substring(0, 12) + "..." : item.title}</b>
                            </span>
                            <div className="fontPanel">Title</div>
                          </Col>
                          <Col span={6}>
                            <div className="icon-style">
                              <AlignCenterOutlined className="icon-display" />
                            </div>
                            <span className="card-title">{item.description.length > 12 ? item.description.substring(0, 12) + "..." : item.description}</span>
                            <div className="fontPanel">Description</div>
                          </Col>
                          <Col span={6}>
                            <div className="icon-style">
                              <ClockCircleOutlined className="icon-display" />
                            </div>
                            <span className="card-title">{item.createdAt}</span>
                            <div className="fontPanel">Created At</div>
                          </Col>
                        </Row>
                      </Col>
                      <Col span={2} style={{ float: "rigth" }}>
                        <div onClick={() => openDrawerUpdate(item)} className="icon-style" style={{}}>
                          <EditOutlined className="icon-display" />
                        </div>
                        <div onClick={() => displayModal(item.id)} className="icon-style">
                          <DeleteOutlined style={{ color: "#d9363e" }} className="icon-display" />
                        </div>
                      </Col>
                    </Row>
                  </>
                ) : (
                  <></>
                )}
                <Modal title="Delete" visible={modalAppear} onCancel={handleCancel} onOk={deleteItem}>
                  <p>Are you sure want to delete this item?</p>
                </Modal>
              </>
            ))}
        </TabPane>

        <TabPane tab="Done" key="2">
          {getDataAPI &&
            getDataAPI.map((item, index) => (
              <>
                {item && item.status === 1 ? (
                  <>
                    <Row className="panel" style={{ cursor: "pointer", padding: "15px" }}>
                      <Col onClick={() => openDrawerDetail(item)} span={22}>
                        <Row>
                          <Col span={3} style={{ paddingLeft: "15px" }}>
                            <div className="icon-style">
                              <FieldNumberOutlined className="icon-display" />
                            </div>
                            <span className="card-title">
                              <b>#{item.id}</b>
                            </span>
                            <div className="fontPanel">User Id</div>
                          </Col>
                          <Col span={6} style={{ paddingLeft: "15px" }}>
                            <div className="icon-style">
                              <HighlightOutlined className="icon-display" />
                            </div>
                            <span className="card-title">
                              <b>{item.title.length > 12 ? item.title.substring(0, 12) + "..." : item.title}</b>
                            </span>
                            <div className="fontPanel">Title</div>
                          </Col>
                          <Col span={6}>
                            <div className="icon-style">
                              <AlignCenterOutlined className="icon-display" />
                            </div>
                            <span className="card-title">{item.description.length > 12 ? item.description.substring(0, 12) + "..." : item.description}</span>
                            <div className="fontPanel">Description</div>
                          </Col>
                          <Col span={6}>
                            <div className="icon-style">
                              <ClockCircleOutlined className="icon-display" />
                            </div>
                            <span className="card-title">{item.createdAt}</span>
                            <div className="fontPanel">Created At</div>
                          </Col>
                        </Row>
                      </Col>
                      <Col onClick={() => openDrawerUpdate(item)} span={2} style={{ float: "right" }}>
                        <div className="icon-style" style={{}}>
                          <EditOutlined className="icon-display" />
                        </div>
                      </Col>
                    </Row>
                  </>
                ) : (
                  <></>
                )}
              </>
            ))}
        </TabPane>
      </Tabs>

      <Button type="primary" shape="circle" icon={<PlusOutlined />} size="large" style={{ position: "fixed", right: 40, bottom: 50, width: 50, height: 50 }} onClick={openDrawerAddItem} />
      <DrawerDetailItem ref={drawerDetailItem} />
      <DrawerUpdateItem ref={drawerUpdateItem} />
      <DrawerAddItem ref={drawerAddItem} />
    </div>
  );
}
