import React, { useState, useImperativeHandle, forwardRef, useEffect } from "react";
import { Drawer, Row, Col, Divider, Form, Input, Select, Button } from "antd";
import { FieldNumberOutlined, ClockCircleOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { updateData } from "redux/action/userAction";

const DrawerUpdateItem = forwardRef((props, ref) => {
  let dispatch = useDispatch();
  const [form] = Form.useForm();

  const [todoListItem, setTodoListItem] = useState();
  const [visible, setVisible] = useState();
  const { getDataAPI } = useSelector((state) => state.data);

  useEffect(() => {
    if (visible) {
      form.resetFields();
    }
  }, [form, visible]);

  useImperativeHandle(ref, () => ({
    showDrawer(data) {
      setTodoListItem(data);
      setVisible(true);
    },
  }));

  const closeDrawer = () => {
    setVisible(false);
  };

  const onFinish = (val) => {
    dispatch(updateData([{ ...val, id: todoListItem && todoListItem.id, createdAt: todoListItem && todoListItem.createdAt }], getDataAPI));
    setVisible(false);
  };

  return (
    <Drawer
      title="Update Item"
      closable={false}
      placement="right"
      visible={visible}
      onClose={closeDrawer}
      width="40%"
      footer={
        <>
          <div style={{ float: "left" }}>
            <Button type="default" onClick={closeDrawer}>
              Cancel
            </Button>
          </div>
          <div style={{ float: "right" }}>
            <Button onClick={form.submit} type="primary">
              Submit
            </Button>
          </div>
        </>
      }
    >
      {todoListItem && (
        <Form form={form} layout="vertical" onFinish={onFinish}>
          <Row>
            <Col span={12} className="itemPanel">
              <div className="icon-style">
                <FieldNumberOutlined className="icon-display" />
              </div>
              <div className="fontPanel">User Id</div>
              <span className="card-title">#{todoListItem.id}</span>
            </Col>
            <Col span={12} className="itemPanel">
              <div className="icon-style">
                <ClockCircleOutlined className="icon-display" />
              </div>
              <div className="fontPanel">Created At</div>
              <span className="card-title">{todoListItem.createdAt}</span>
            </Col>
          </Row>
          <Divider style={{ color: "rgba(0,0,0,.5)" }} orientation="left">
            <b>Update Data</b>
          </Divider>
          <Form.Item label="Title" initialValue={todoListItem.title} name="title">
            <Input />
          </Form.Item>
          <Form.Item label="Description" initialValue={todoListItem.description} name="description">
            <Input />
          </Form.Item>
          <Form.Item label="Status" initialValue={todoListItem.status} name="status">
            <Select
              defaultValue={todoListItem.status}
              placeholder="Change Status"
              optionFilterProp="children"
              filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Select.Option value={0}>Active</Select.Option>
              <Select.Option value={1}>Done</Select.Option>
            </Select>
          </Form.Item>
        </Form>
      )}
    </Drawer>
  );
});

export default DrawerUpdateItem;
