import axios from "axios";
import { GET_DATA_LIST, GET_DELETE, GET_UPDATE, GET_ADD } from "./helper";

export const getData = () => {
  return (dispatch) => {
    axios
      .get("https://virtserver.swaggerhub.com/hanabyan/todo/1.0.0/to-do-list")
      .then(function (response) {
        dispatch({
          type: GET_DATA_LIST,
          payload: {
            data: response.data,
            errorMessage: false,
          },
        });
      })
      .catch(function (error) {
        dispatch({
          type: GET_DATA_LIST,
          payload: {
            data: false,
            errorMessage: error.message,
          },
        });
      });
  };
};

export const updateData = (data, data_old) => {
  const newData = [];
  data_old.map((index) => {
    newData.push(
      data[0].id === index.id
        ? {
            createdAt: data[0].createdAt,
            description: data[0].description,
            id: data[0].id,
            status: data[0].status,
            title: data[0].title,
          }
        : index,
    );
  });

  return (dispatch) => {
    dispatch({
      type: GET_UPDATE,
      payload: {
        data: newData,
      },
    });
  };
};

export const addData = (data, data_old) => {
  data_old.push({
    id: data[0].id,
    title: data[0].title,
    description: data[0].description,
    status: data[0].status,
    createdAt: data[0].createdAt,
  });

  return (dispatch) => {
    dispatch({
      type: GET_ADD,
      payload: {
        data: data_old,
      },
    });
  };
};

export const deleteData = (id, data_old) => {
  const findIndex = data_old.findIndex((a) => a.id === id);
  const newData = [];
  findIndex !== -1 && data_old.splice(findIndex, 1);
  data_old.map((index) => {
    newData.push(index);
  });
  return (dispatch) => {
    dispatch({
      type: GET_DELETE,
      payload: {
        data: newData,
      },
    });
  };
};
