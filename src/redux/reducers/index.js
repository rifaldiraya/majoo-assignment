import { combineReducers } from "redux";
import data from "redux/reducers/data";

export default combineReducers({
  data,
});
