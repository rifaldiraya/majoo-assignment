import { GET_DATA_LIST, GET_DELETE, GET_UPDATE, GET_ADD, GET_REVERSE } from "redux/action/helper";

let initialState = {
  getDataAPI: false,
  errorDataList: false,
};

const data = (state = initialState, action) => {
  switch (action.type) {
    case GET_DATA_LIST:
      return {
        ...state,
        getDataAPI: action.payload.data,
        errorDataList: action.payload.errorMessage,
      };
    case GET_UPDATE:
      return {
        ...state,
        getDataAPI: action.payload.data,
      };
    case GET_DELETE:
      return {
        ...state,
        getDataAPI: action.payload.data,
      };
    case GET_ADD:
      return {
        ...state,
        getDataAPI: action.payload.data,
      };
    default:
      return state;
  }
};

export default data;
